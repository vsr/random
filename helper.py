# -*- coding: utf-8 -

import os
import requests
import json
import time

from settings import WORDNIK_API_KEY


def edited_recently(file, secs):
    """ Was the `file` edited in last `secs` seconds? """

    return os.path.exists(file) and (os.path.getmtime(file)+secs) > time.time()


def list_difference(l1, l2):
    return [x for x in l1 if x not in l2]

def get_wordlist(count=100):

    wordlist = ["hello", "cricket", "player", "game"]
    params = {"hasDictionaryDef": False, "includePartOfSpeech": "noun,adjective,verb,idiom",
        "minCorpusCount": 0, "maxCorpusCount": -1, "minDictionaryCount": 1,
        "maxDictionaryCount": -1, "minLength": 3, "maxLength": -1, "limit": count,
        "api_key": WORDNIK_API_KEY }

    response = requests.get("http://api.wordnik.com:80/v4/words.json/randomWords", params=params)

    if response.status_code == 200:
        result = json.loads(response.content)
        wordlist = []
        for word_object in result:
            wordlist.append(word_object["word"])
    else:
        raise Exception("Wordnik API respone fail! %s"%response.status_code)

    return wordlist

