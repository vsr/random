if [ "$1" = "-runserver" ] ; then

    ../../bin/gunicorn -b 127.0.0.1:8009 -w 4 --max-requests 1000 home

fi

if [ "$1" = "-build" ] ; then

    export VER=`cat version.txt`
    echo "Building secrets $VER"
    # lessc and uglify(https://npmjs.org/package/uglifyjs) node modules need to be installed globally
    # npm install -g less
    mkdir www/css -p
    rm www/css/*.css
    lessc -compress src/www/css/style.less > www/css/style-$VER.css
    cp src/www/*.html www/

fi

if [ "$1" = "-fetch" ] ; then

    ../../bin/python fetch.py

fi

if [ "$1" = "-rundbserver" ] ; then

    export DATA_PATH=/tmp/
    ../../bin/gunicorn -b 127.0.0.1:8055 -w 1 --max-requests 1000 dbserver

fi
