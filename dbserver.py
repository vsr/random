# -*- coding: utf-8 -*-
"""
    HTTP key-value database server
"""

import os
import json
from bottle import get, post, abort, error, app, request, template
import signal
from threading import Timer
import logging

DEFAULT_DATA = {}
SAVE_INTERVAL = 30

DATA_PATH = os.getenv("DATA_PATH", None)

if not DATA_PATH:
    raise Exception("DATA_PATH environment variable not set.")
    exit()

DB_FILE = os.path.join(DATA_PATH, 'testdbfile.db')
DATA = DEFAULT_DATA

time_int = None
is_dirty = False


def save_data():
    """ Persist data to file """
    logging.debug("saving data")
    global is_dirty, DB_FILE, DATA
    with open(DB_FILE, "w") as f:
        json.dump(DATA, f)
    is_dirty = False

def read_data():
    """ Read data from file """
    logging.debug("Reading data")
    global is_dirty, DB_FILE, DATA
    with open(DB_FILE, "r") as f:
        try:
            DATA = json.load(f)
        except ValueError:
            DATA = DEFAULT_DATA
    is_dirty = False

def recurring_save():
    """ Recurrently persist data if dirty """
    logging.debug("Recurring save")
    global time_int, is_dirty, DB_FILE, DATA
    time_int = Timer(SAVE_INTERVAL, recurring_save, ())
    time_int.start()
    if is_dirty:
        save_data()

def onexit(signum, frame):
    """ Persist and exit """
    logging.warn("Preparing to exit")
    global time_int
    if time_int: time_int.cancel()
    save_data()
    logging.warn("DB server exiting..")
    exit()


@get("/<key_name>")
def get_data(key_name):
    """ Get data for key """
    logging.debug("Get key: %s" % key_name)
    global DATA
    if key_name in DATA:
        return DATA[key_name]
    else:
        abort(404, "No data")

@post("/<key_name>")
def set_data(key_name):
    """ Set data for key """
    logging.debug("Set key: %s" % key_name)
    global DATA, is_dirty
    content = request.body.read()
    DATA[key_name] = content
    is_dirty = True
    return 'OK'

@error(404)
def error_route(message):
    """ Simple error route """
    return template("Not found: {{message}}", message=message)


logging.info("DB server starting..")
application = app()
application.catchall = False

# Persist data on keyboard interrupts, term signals
signal.signal(signal.SIGINT, onexit)
signal.signal(signal.SIGTERM, onexit)


# initialize data
if not os.path.exists(DB_FILE):
    save_data()
else:
    read_data()
recurring_save()
