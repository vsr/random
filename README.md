# random #

Simple webapp to display random content from social content sites- for fun and inspiration.

### Setup ###

* Get virtualenv, create a virtualenv and install all python library dependencies from `requirements.txt`.
* Move `settings.sample.py` to `settings.py` and fill in API keys.
* Use `./commands.sh -fetch` to fetch content. 
* Use `./commands.sh -build` to build static files. It compiles & moves files from `src` to `www` directory. [warning: You'll need node.js and lesscss.]
* Use `commands.sh -runserver` flag to run webapp using gunicorn.
