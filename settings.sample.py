# -*- coding: utf-8 -
import os
import logging
from logging import config

LOG_FILE = '../log/error.log'
LOG_LEVEL = 'INFO'

ENCODING = 'utf8'

APP_PATH = os.path.dirname(__file__)
APP_VERSION = open(os.path.join(APP_PATH, 'version.txt'),'r').read().strip()

DBSERVER = "http://localhost:8055/"

MAX_ITEMS = 100
MIN_ITEMS = 30
MAX_VIEW_COUNT = 5
UPDATE_INTERVAL = 60*10

WORDNIK_API_KEY = "get api key from http://developer.wordnik.com/"

CONTENT_MODULES = {
    "youtube": {
        "enabled": True,
        "DEVELOPER_KEY": "get api key from google dev console https://console.developers.google.com/project",
        "YOUTUBE_API_SERVICE_NAME": "youtube",
        "YOUTUBE_API_VERSION": "v3",
        "max_items": 100,
        "min_items": MIN_ITEMS,
        "update_interval": 60*2
    },
    "twitter": {
        "enabled": False,
        "CONSUMER_KEY": "Get consumer key, secret from dev.twitter.com",
        "CONSUMER_SECRET": "",
        "max_items": MAX_ITEMS,
        "min_items": MIN_ITEMS,
        "update_interval": UPDATE_INTERVAL
    },
    "imgur": {
        "enabled": False,
        "CLIENT_ID": "Get imgur client id",
        "max_items": MAX_ITEMS,
        "min_items": MIN_ITEMS,
        "update_interval": UPDATE_INTERVAL
    },
    "gplus": {
        "enabled": False,
        "DEVELOPER_KEY": "same old google api key",
        "GPLUS_API_SERVICE_NAME": "plus",
        "GPLUS_API_VERSION": "v1",
        "max_items": MAX_ITEMS,
        "min_items": MIN_ITEMS,
        "update_interval": UPDATE_INTERVAL
    },
    "books": {
        "enabled": True,
        "DEVELOPER_KEY": "same old google api key",
        "BOOKS_API_SERVICE_NAME": "books",
        "BOOKS_API_VERSION": "v1",
        "max_items": MAX_ITEMS,
        "min_items": MIN_ITEMS,
        "update_interval": UPDATE_INTERVAL
    }
}



logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'verbose2': {
            'format': '%(levelname)s %(asctime)s %(module)s %(filename)s  %(lineno)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
#        'email': {
#            'level': 'ERROR',
#            'formatter': 'verbose2',
#            'class': 'logging.handlers.SMTPHandler'
#        },
        'rotating_file': {
            'level': LOG_LEVEL,
            'formatter': 'verbose2',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': LOG_FILE,
            'when': 'midnight',
            'interval': 1,
            'backupCount': 7,
        }
    },
    'loggers': {
        '': {
            'level': LOG_LEVEL,
            'handlers': ['rotating_file'],
            'propagate': False,
        }
    }
})

