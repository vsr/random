# -*- coding: utf-8 -

import os
from helper import get_wordlist
from content import Content
import logging

class youtube(Content):
    _MODULE_NAME = "youtube"

    def templateHTML(self):
        return """
        <div style="float: none;clear: both;width: 100%;position: relative;padding-bottom: 56.25%;padding-top: 25px;height: 0;">
          <iframe id="ytplayer" type="text/html" style="position: absolute;top:0;left:0; width: 100%;height: 100%;" src="http://www.youtube.com/embed/{{content["id"]["videoId"]}}?autoplay=0" frameborder="0"></iframe>
        </div>
        <h3>{{content["snippet"]["title"]}}</h3>
        <p>{{content["snippet"]["description"]}}</p>
        """

    def fetch_content(self):
        super(self.__class__, self).fetch_content()

        from apiclient.discovery import build
        from apiclient.errors import HttpError

        items_count = len(self._data["items"])
        wordlist = get_wordlist(10)
        max_results = 2
        orders = ["date", "rating", "relevance", "title", "viewCount"]

        yt = build(self._options.get("YOUTUBE_API_SERVICE_NAME"), self._options.get("YOUTUBE_API_VERSION"),
            developerKey=self._options.get("DEVELOPER_KEY"))

        for word in wordlist:
            query = word

            # Call the search.list method to retrieve results matching the specified
            # query term.
            search_response = yt.search().list(
                q=query,
                part="id,snippet",
                maxResults=max_results,
                videoEmbeddable="true",
                type="video",
                order=self._rand_generator.choice(orders)
            ).execute()

            for search_result in search_response.get("items", []):
                if search_result["id"]["kind"] == "youtube#video":
                    self.add_item(search_result)

        self.save()
        logging.info("Updated %s from %s to %s."%(self._MODULE_NAME, items_count, len(self._data["items"])))
        return
