# -*- coding: utf-8 -

import os
import json
from random import Random
import operator
from bottle import jinja2_template as template
import logging
import requests

from settings import MAX_ITEMS, MIN_ITEMS, MAX_VIEW_COUNT


class Content(object):
    _MODULE_NAME = 'content'

    def __init__(self, dbserver, options):
        self._db = dbserver
        self._options = options
        self._rand_generator = Random()
        self._rand_generator.seed(os.urandom(12)+os.urandom(12))

        try:
            self.read()
        except (ValueError, KeyError):
            logging.exception('')
            logging.error("Error reading data for %s"%self._MODULE_NAME)
            self._data = { "items": []}
            self.save()
        logging.info("Initialized module for %s"%self._MODULE_NAME)


    def read(self):
        try:
            response = requests.get(self._db+self._MODULE_NAME)
        except requests.ConnectionError as e:
            logging.exception('')
            logging.error("Could not connect to DB server")
            raise e
        if response.status_code == 200:
            self._data = json.loads(response.content)
        elif response.status_code == 404:
            raise KeyError()
        else:
            logging.error("Problem getting data from DB server: %s\n%s"%(response.status_code, response.content))
            raise Exception("Problem with DB server")


    def save(self):
        try:
            response = requests.post(self._db+self._MODULE_NAME, data=json.dumps(self._data))
        except requests.ConnectionError as e:
            logging.exception('')
            logging.error("Could not connect to DB server")
            raise e
        if response.status_code == 200:
            logging.debug("Saved data")
        else:
            logging.error("Problem saving data from DB server: %s\n%s", (response.status_code, response.content))
            raise Exception("Problem with DB server")

    def add_item(self, item):
        self._data["items"].append( (item, 0) )

    def items(self):
        self.read()
        return self._data["items"]

    def length(self):
        return len(self.items())

    def is_full(self):
        self.trim_items()
        max_items = self._options.get("max_items", MAX_ITEMS)
        return self.length() >= max_items

    def trim_items(self):
        self.read()
        max_view_count = self._options.get("max_view_count", MAX_VIEW_COUNT)
        min_items = self._options.get("min_items", MIN_ITEMS)
        items_length = len(self._data["items"])
        logging.info("before trim: %s"%items_length)

        trimmed_list = [item for item in self._data["items"] if item[1]<=max_view_count]
        trim_list_length = len(trimmed_list)
        logging.info("list of view counts: %s"%  [item[1] for item in self._data["items"]])

        if trim_list_length < min_items:
            trimmed_items = [item for item in self._data["items"] if item[1]>max_view_count]
            trimmed_items.sort(key=operator.itemgetter(1))
            trimmed_list.extend(trimmed_items[:(min_items-trim_list_length)])

        self._data["items"] = trimmed_list
        logging.info("after trim: %s"%len(self._data["items"]))
        self.save()

    def templateHTML(self):
        return ""

    def random_item(self):
        self.read()

        if len(self._data["items"]) == 0:
            logging.error("No items present %s"%self._MODULE_NAME)
            return "Sorry, we seem to have run out of content!"

        try:
            rand_item_index = self._rand_generator.randrange(0, len(self._data["items"]))
            content, view_count = self._data["items"][rand_item_index]
            self._data["items"][rand_item_index] = (content, view_count+1)
            self.save()
            logging.info("Random item for %s"%self._MODULE_NAME)
        except IndexError, e:
            logging.error("Some error fetching random item:")
            logging.exception(e)
            return "OOPS! Nothing in here."
        return template(self.templateHTML(), content=content)

    def fetch_content(self):
        self.trim_items()

