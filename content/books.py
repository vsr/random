# -*- coding: utf-8 -

import os
from helper import get_wordlist
from content import Content
import logging

class books(Content):
    _MODULE_NAME = "books"

    def templateHTML(self):
        return """
        <div class="t-a-l">
            <div >
                <h1>
                    <a target="_gbook" href="{{content.volumeInfo.canonicalVolumeLink}}">
                    {{content.volumeInfo.title}}
                    </a>
                    {% if content.volumeInfo.subtitle %}<small>:{{content.volumeInfo.subtitle}}</small>{% endif %}
                </h1>
            </div>
            <div>
                {% if content.volumeInfo.imageLinks and content.volumeInfo.imageLinks.thumbnail %}
                    <img style="margin: 0 2em 1em 0;" align="left" src="{{content.volumeInfo.imageLinks.thumbnail}}"/>
                {% endif %}
                {% if content.volumeInfo.authors %}
                    <p>{{content.volumeInfo.authors|join(', ')}}</p>
                {% endif %}
                <p>{{content.volumeInfo.publisher}} - {{content.volumeInfo.publishedDate}}</p>
                {% if content.volumeInfo.categories %}
                    <p>{{",".join(content.volumeInfo.categories)}}</p>
                {% endif %}
                <p>{{content.volumeInfo.description}}</p>
                {% if content.volumeInfo.industryIdentifiers %}
                <div>
                <span>{{content.volumeInfo.industryIdentifiers|join(', ', attribute='identifier')}}</span>
                {% for ids in content.volumeInfo.industryIdentifiers %}{% endfor %}
                    {% for ids in content.volumeInfo.industryIdentifiers if ids.type=='ISBN_10' %}
                        <script type="text/javascript" src="http://books.google.com/books/previewlib.js"></script>
                        <script type="text/javascript">
                        GBS_insertPreviewButtonPopup('ISBN:{{ids.identifier}}');
                        </script>
                    {% endfor %}
                </div>
                {% endif %}
            </div>
        </div>

        """


    def fetch_content(self):
        super(self.__class__, self).fetch_content()

        from apiclient.discovery import build
        from apiclient.errors import HttpError

        items_count = len(self._data["items"])
        wordlist = get_wordlist(10)
        max_results = 3

        orderBy = ["newest", "relevance"]

        book_api = build(self._options.get("BOOKS_API_SERVICE_NAME"), self._options.get("BOOKS_API_VERSION"),
            developerKey=self._options.get("DEVELOPER_KEY"))

        for word in wordlist:
            self._rand_generator.seed(os.urandom(12)+os.urandom(12))

            # Call the search.list method to retrieve results matching the specified
            # query term.
            search_response = book_api.volumes().list(
                q=word,
                printType="books",
                maxResults=max_results,
                orderBy=self._rand_generator.choice(orderBy),
                startIndex=self._rand_generator.choice(range(10)),
                fields="items(id,kind,volumeInfo),kind,totalItems"
            ).execute()

            for search_result in search_response.get("items", []):
                if search_result["kind"] == "books#volume":
                    self.add_item(search_result)

        self.save()
        logging.info("Updated %s from %s to %s."%(self._MODULE_NAME, items_count, len(self._data["items"])))
        return
