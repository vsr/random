# -*- coding: utf-8 -

import os
import json
from helper import get_wordlist
from content import Content
import logging

class imgur(Content):
    _MODULE_NAME = 'imgur'
    _API_URL = "https://api.imgur.com/3/gallery/search/%s/%s/%s"

    def templateHTML(self):
        return """
        <a href="{{content["link"]}}" title="{{content["title"]}}" target="_imgur">
        <img style="max-width: 100%;" src="{{content["link"]}}" alt="{{content["title"]}}" title="Hosted by imgur.com" />
        </a>
        <p>{{content["title"]}}</p>
        """

    def fetch_content(self):
        super(self.__class__, self).fetch_content()

        import requests

        items_count = len(self._data["items"])
        wordlist = get_wordlist(20)
        max_results = 7

        sort = ["time"] #, "viral"]
        window = ["month", "year", "all"] # "day", "week",


        for word in wordlist:

            params = {"q": word}
            headers = {"Authorization": "Client-ID "+self._options.get("CLIENT_ID","")}
            url = self._API_URL%( self._rand_generator.choice(sort),
                             self._rand_generator.choice(window), 0 )

            response = requests.get(url, params=params, headers=headers)

            if response.status_code == 200:
                search_response = json.loads(response.content)
                max_results_count = 0
                for search_result in search_response.get("data", []):
                    if not search_result["is_album"] and not search_result["nsfw"]:
                        self.add_item(search_result)
                        max_results_count += 1
                    if max_results_count >= max_results:
                        break

        self.save()
        logging.info("Updated %s from %s to %s."%(self._MODULE_NAME, items_count, len(self._data["items"])))
        return
