# -*- coding: utf-8 -

import os
import json
from helper import get_wordlist
from content import Content
import logging

class twitter(Content):
    _MODULE_NAME = 'twitter'
    _API_URL = "https://api.twitter.com/1.1/search/tweets.json"
    _REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth2/token'

    def templateHTML(self):
        return """
        <blockquote align="center" class="twitter-tweet" lang="{{content["lang"]}}"><p>{{content["text"]}}</p>&mdash; {{content["user"]["name"]}} (@{{content["user"]["screen_name"]}}) <a href="https://twitter.com/{{content["user"]["screen_name"]}}/statuses/{{content["id_str"]}}">{{content["created_at"]}}</a></blockquote>
        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        """

    def get_access_token(self):

        import base64, requests

        bearer_token = "%s:%s"%(self._options.get("CONSUMER_KEY"), self._options.get("CONSUMER_SECRET"))
        encoded_bearer_token = base64.b64encode(bearer_token.encode('ascii'))

        data = "grant_type=client_credentials"
        headers = {
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            "Authorization": "Basic %s" % encoded_bearer_token.decode('utf-8')
        }

        response = requests.post(self._REQUEST_TOKEN_URL, data=data, headers=headers)
        if response.status_code == 200:
            content = json.loads(response.content)
            return content["access_token"]
        raise Exception("Error fetching access token. response is %s, %s"%(response.status_code, response.content))


    def fetch_content(self):
        super(self.__class__, self).fetch_content()

        import requests


        items_count = len(self._data["items"])
        wordlist = get_wordlist(20)
        max_results = 7

        access_token = self.get_access_token()
        url = self._API_URL

        for word in wordlist:

            params = {"q": word, "count": max_results, "include_entities": False}
            headers = {"Authorization": "Bearer %s"%access_token}

            response = requests.get(url, params=params, headers=headers)

            if response.status_code == 200:
                search_response = json.loads(response.content)
                for search_result in search_response.get("statuses", []):
                    self.add_item(search_result)

        self.save()
        logging.info("Updated %s from %s to %s."%(self._MODULE_NAME, items_count, len(self._data["items"])))
        return
