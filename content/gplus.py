# -*- coding: utf-8 -

import os
from helper import get_wordlist
from content import Content
import logging

class gplus(Content):
    _MODULE_NAME = "gplus"

    def templateHTML(self):
        return """
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
        <div class="g-post" data-href="{{content["url"]}}">
            <p>{{content["title"]}}</p>
        </div>
        """

    def fetch_content(self):
        super(self.__class__, self).fetch_content()

        from apiclient.discovery import build
        from apiclient.errors import HttpError

        items_count = len(self._data["items"])
        wordlist = get_wordlist(10)
        max_results = 3

        plus = build(self._options.get("GPLUS_API_SERVICE_NAME"), self._options.get("GPLUS_API_VERSION"),
            developerKey=self._options.get("DEVELOPER_KEY"))

        for word in wordlist:
            self._rand_generator.seed(os.urandom(12)+os.urandom(12))
            query = word

            # Call the search.list method to retrieve results matching the specified
            # query term.
            search_response = plus.activities().search(
                query=query,
                maxResults=max_results
            ).execute()

            for search_result in search_response.get("items", []):
                if search_result["kind"] == "plus#activity":
                    self.add_item(search_result)

        self.save()
        logging.info("Updated %s from %s to %s."%(self._MODULE_NAME, items_count, len(self._data["items"])))
        return
