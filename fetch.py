# -*- coding: utf-8 -*-
"""
    Fetch content for enabled modules.
"""


from settings import CONTENT_MODULES, DBSERVER
import logging

ENABLED_MODULES = [k for k in CONTENT_MODULES if CONTENT_MODULES[k].get('enabled', False)]


if __name__ == '__main__':

    for module_name in ENABLED_MODULES:
        options = CONTENT_MODULES[module_name]
        module_class = getattr(__import__("content.%s"%module_name, globals(), locals(), [module_name,], -1), module_name)
        module = module_class(DBSERVER, options)
        if not module.is_full():
            logging.info("Fetching %s"%module_name)
            module.fetch_content()
        else:
            logging.warn("We are full: %s"%module_name)
