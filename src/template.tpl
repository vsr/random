<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>random</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Random content from Youtube, Imgur, Twitter, Google+, Google books - for fun!">
    <meta name="keywords" content="random, youtube, twitter">
    <link href="static/css/style-{{APP_VERSION}}.css" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico">
    <!--[if IE]><script>var e="abbr,article,aside,audio,canvas,datalist,details,figure,footer,header,hgroup,mark,menu,meter,nav,output,progress,section,time,video".split(',');var i=e.length;while(i--){document.createElement(e[i]);}</script><![endif]-->
    {% include "google_analytics.tpl" %}
  </head>
  <body class="container">
      <header>
          <h1><a href="/" title="Random- reload to see another random content.">random</a></h1>
          <a class="reload-link" href="">reload</a>
      </header>
      <section class="content">
{%if error %}
    <p class="error">{{ message }}</p>
{% else %}
    {{ content_html }}
{% endif %}
      </section>
<script>
    (function() {
      WebFontConfig = {
        google: {
          families: ['Open Sans']
        }
      };
      var wf = document.createElement('script');
      wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js';
      wf.type = 'text/javascript'; wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })();
  </script>
      <section class="module-nav">
        <ul>
            <li><a href="/">all</a></li>
            {% for m in ENABLED_MODULES %}
              <li><a {%if path==m %}class="selected"{% endif %}href="/{{m}}">{{m}}</a></li>
            {% endfor %}
        </ul>
      </section>
      <section class="adv">
          <div class="">
            {% include "advert.tpl" %}
          </div>
      </section>
      <footer>
        <p>Random content from Youtube, Imgur, Twitter, Google+, Google books - for fun!</p>
      </footer>
  </body>
</html>
