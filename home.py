# -*- coding: utf-8 -*-

import os
from bottle import get, abort, error, app, static_file, route, request
from bottle import jinja2_template as template
from bottle import TEMPLATE_PATH
from settings import *
from random import Random
import logging

TEMPLATE_PATH[:] = ['src']

ENABLED_MODULES = [k for k in CONTENT_MODULES if CONTENT_MODULES[k].get('enabled', False)]

global_context = {"APP_VERSION": APP_VERSION, "ENABLED_MODULES": ENABLED_MODULES}

module_objects = {}

for m in ENABLED_MODULES:
    module_class = getattr(__import__("content.%s"%m, globals(), locals(), [m,], -1), m)
    module_objects[m] = module_class(DBSERVER, CONTENT_MODULES[m])

rand_generator = Random()
rand_generator.seed(os.urandom(12)+os.urandom(12))


@route('/static/<filename:path>')
def send_static(filename):
    return static_file(filename, root=os.path.join(APP_PATH,'www'))


@get('/')
def home():
    global global_context
    module = rand_generator.choice(module_objects.values())
    html = module.random_item()
    context = dict({"content_html": html, "module_name": module._MODULE_NAME}, **global_context)
    logging.info("Home page, showing %s"%module._MODULE_NAME)
    return template('template', context)


@get('/<module_names>')
def module_page(module_names):
    global global_context
    path = module_names
    module_names_list = module_names.split("+")

    selected_modules = [module_objects[k] for k in module_objects if k in module_names_list]

    if not selected_modules:
        abort(404, "Content not found")

    module = rand_generator.choice(selected_modules)
    html = module.random_item()
    logging.info("Module page, showing %s"%module._MODULE_NAME)
    context = dict({"content_html": html, "module_name": module._MODULE_NAME, "path": path}, **global_context)
    return template('template', context)



@error(404)
def notfound(notfound):
    global global_context
    logging.error("Not found %s"%request.fullpath)
    context = dict({"error": True, "message": "Could not find what you were looking for."},
        **global_context)
    return template('template', context)


@error()
def catchall(error_status):
    global global_context
    logging.error("Major error %s"%error_status)
    logging.exception(error_status)
    context = dict({"error": True, "message": "OOPS! Sorry, something went wrong on our end."},
        **global_context)
    return template('template', context)



logging.info("Application starting.")
application = app()
application.catchall = False